Compile:
1. 打make直接使用Makefile來compile
2. 或者打入以下兩行來compile
    gcc server.c -o server
    gcc -pthread client.c -o rsh-client

Execute:
1. 先在伺服器(linux7.csie.ntu.edu.tw)執行
    ./server
2. 再來開本機連線該伺服器的port 9898
    ./rsh-client linux7.ntu.edu.tw 9898
   連線成功後會出現welcome message
