#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <pthread.h>

#define BUFFER_SIZE 1024

int sockfd;
int isNumber(char *str){
    char *p;
    strtod( str, &p);
    return *p=='\0';
}
void *readFromServer(){
    char buffer[BUFFER_SIZE]="";
    int n, i;
    while(1){
        bzero( buffer, sizeof(buffer));
        n = read( sockfd, buffer, sizeof(buffer));
        for(i=0;i<n;i++) printf("%c",buffer[i]);
    }
}
int main(int argc, char *argv[])
{
    if( argc!=3 ){
        fprintf(stderr, "You must give 3 argument! %s host port.\n", argv[0]);
        return -1;
    }
    char buffer[BUFFER_SIZE];
    struct sockaddr_in serverAddress;
    struct hostent *host;
    struct in_addr **address;
    pthread_t thread;

    if( (host = gethostbyname(argv[1])) == NULL){
        fprintf(stderr, "gethostbyname error!\n");
        return -1;
    }
    if( !isNumber(argv[2]) ){
        fprintf(stderr, "Wrong type of port!\n");
    }
    address = (struct in_addr **)host->h_addr_list;
    char *ipAddress = inet_ntoa(*address[0]);

    sockfd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons( atoi(argv[2]) );
    inet_pton(AF_INET, ipAddress, &serverAddress.sin_addr);
    connect(sockfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress));

    pthread_create(&thread, NULL, readFromServer, NULL); // create a thread to handle reading
    int numOfStr = 0, readNum = 0, i, n;
    while(1){
        fgets(buffer, BUFFER_SIZE, stdin); // get the instruction from stdin
        write( sockfd, buffer, sizeof(buffer) ); // write to server through socket
    }
    pthread_join(thread, NULL);
    close(sockfd);
    return 0;
}
