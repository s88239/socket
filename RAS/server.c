#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <time.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>

#define BUFFER_SIZE 1024

typedef struct listNode
{
    int round;
    char *fileName;
    struct listNode *next;
} waitNode;
typedef waitNode *waitPtr;
void cutString(char *, waitPtr *, waitPtr);
char *cutByRedirect(char *);
void connectPipe(waitPtr, char **, int, int, char *, waitPtr);
void judgeCommand(char *);
void redirect(char *);
void pipeNum(waitPtr);
void earlyPipe(waitPtr *, int[]);
int isNumber(char *);
char *getName(int);
waitPtr insertList(waitPtr *, int );
waitPtr popList(waitPtr *); 
void updateList(waitPtr *);

int connfd; // socket connect file descriptor
int main(int argc, char *argv[])
{
    char buffer[BUFFER_SIZE] = "";
    int listenfd;//, connfd;
    socklen_t length;
    struct sockaddr_in serverAddress, clientAddress;
    listenfd = socket(AF_INET, SOCK_STREAM, 0);
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(9898); // my server port
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    bind(listenfd, (struct sockaddr *) &serverAddress, sizeof(serverAddress));
    listen(listenfd, 3); // at most 3 people can connect server simultaneously
    char welcomeMes[] = "******************************************\n*Welcome to the workstation of b00902098.*\n******************************************\n";
    while(1) {
        length = sizeof(clientAddress);
        connfd = accept(listenfd, (struct sockaddr *) &clientAddress, &length);
        pid_t pid = fork();
        if( pid < 0 ){
            fprintf(stderr, "fork error!\n");
        }
        else if( pid == 0 ){ // child process
            putenv( "PATH=bin:."); // initialize the environment variable 'PATH'
            dup2( connfd , STDOUT_FILENO); // redirect stdout to socket
            dup2( connfd , STDERR_FILENO); // redirect stderr to socket
            write(connfd, welcomeMes, strlen(welcomeMes) ); // write welcome message to client 
            waitPtr head = NULL, goalNode = NULL;
            char *cpyPath="";
            while(1){
                bzero(buffer, sizeof(buffer));
                read(connfd, buffer, sizeof(buffer)); // read from client
                updateList( &head ); // round - 1
                goalNode = popList( &head); // get the node with round=0
                if(goalNode!=NULL){
                    while( goalNode!=NULL ){ // pop all the nodes with expired round
                        cpyPath = (char*)malloc( (strlen(buffer)+1)*sizeof(char) );
                        strcpy( cpyPath, buffer);
                        cutString( cpyPath, &head , goalNode); // connect the current instruction
                        goalNode = popList( &head );
                    }
                }
                else cutString( buffer , &head , goalNode); // cut string and execute the command
            }
        }
        else{
            close(connfd); // close socket in parent and wait for next user connecting
            //wait();
        }
    }
}
void cutString(char *line, waitPtr *head, waitPtr goalNode){ // cut string by pipe
    const char delimeter[]="|\n";
    char *strSegment = strtok( line, delimeter);
    char *pipeList[100]={NULL};
    char *fileStr = NULL;
    char *previousResult = NULL;
    int count = 0;
    waitPtr inNode = NULL;
    for(count=0;strSegment!=NULL;count++ ){
        pipeList[count] = strSegment; // store command waiting for pipe
        if( strstr(pipeList[count],"setenv") ){ // do setenv command fist to prevent from doing in the child process instead of parent process
            char *newPath = (char*)malloc( (strlen(pipeList[count])+1)*sizeof(char) );
            strcpy( newPath, pipeList[count]);
            judgeCommand( newPath );
        }
        strSegment = strtok( NULL, delimeter);
    }
    if(count>0){
        fileStr = cutByRedirect( pipeList[count-1] ); // If there is a redirection, cut and return the file name
        if( !fileStr && isNumber( pipeList[count-1] ) ){ // pipe num
            inNode = insertList( head, atoi(pipeList[count-1])); // insert to list
            count--;
        }
    }
    connectPipe( inNode, pipeList, -1, count-1, fileStr, goalNode);
}
char *cutByRedirect(char *string){ // cut string by redirect symbol
    if( !strchr(string, '>') ) return NULL;
    const char delimeter[]=">";
    char *strSegment = strtok( string, delimeter);
    char *fileStr = NULL;
    while( strSegment != NULL ){
        fileStr = strSegment;
        strSegment = strtok( NULL, delimeter);
    }
    return fileStr==NULL ? NULL : strtok( fileStr, " ");// remove space
}
void connectPipe(waitPtr inNode, char **ins, int current, int count, char *fileStr, waitPtr goalNode){
    int pipefd[2];
    if( pipe(pipefd)<0) fprintf(stderr, "pipe error!\n");
    pid_t pid = fork();
    if( pid < 0 ) fprintf(stderr, "fork error!\n");
    else if( pid > 0){// parent
        if( !goalNode ){
            close( pipefd[0] ); // close reading end of pipe
            dup2( pipefd[1], STDOUT_FILENO ); // redirect writing end of pipe to STDOUT
            close( pipefd[1] ); // close pipe
        }
        else earlyPipe( &goalNode, pipefd); // pipe num early is due
        if(current>=0) judgeCommand( ins[current]);
        wait();
    }
    else{ // child
        close( pipefd[1] ); // close writing end of pipe
        dup2( pipefd[0] , STDIN_FILENO); // redirect reading end of pipe to STDIN
        close( pipefd[0] ); // close reading end of pipe
        current ++;
        if( current == count ){//connect competely
            if( fileStr!=NULL ){ // redirect to the file
                redirect( fileStr );
            }
            else if( inNode ){ // pipe num
                pipeNum( inNode);
            }
            else{ // general instruction
                dup2( connfd, STDOUT_FILENO); // redirect STDOUT to socket
                close( connfd );
            }
            judgeCommand( ins[current]);
        }
        else connectPipe( inNode, ins, current, count, fileStr, goalNode);
        exit(0);
    }
}
void judgeCommand(char *line){
    const char delimeter[] = " "; // cut the string by space
    char *processStr = (char*)malloc( (strlen(line)+1)*sizeof(char) );
    strcpy( processStr, line); // let processStr be a copy of line
    char *strCommand = strtok( processStr, delimeter);// catch the command
    char *strSegment = strCommand;
    char *segList[100]={NULL};
    char *strPath=NULL;
    int count = 0;
    for( count=0; strSegment!=NULL; count++ ){
        segList[count] = strSegment;
        strSegment = strtok( NULL, delimeter);
        if( strchr(segList[count], '\'') ){ // omit the single quote
            segList[count] = strtok( segList[count], "\'");
        }
        else if( strchr(segList[count], '\"') ){ // omit the double quote
            segList[count] = strtok( segList[count], "\"");
        }
    }
    if( strcmp( strCommand, "printenv")==0 ){
        if( strcmp( segList[1], "PATH")==0 ){
            strPath = getenv(segList[1]);
            fprintf( stdout, "%s%c%s\n", segList[1], '=', strPath);
        }
        else{
            fprintf( stderr, "%s %s: Command not found.\n", strCommand, segList[1]);
        }
    }
    else if( strcmp( strCommand, "setenv")==0 ){
        if( strcmp( segList[1], "PATH")==0 ){ // set the path to environment
            setenv( segList[1], segList[2], 1);
        }
        else{
            fprintf( stderr, "%s %s: Command not found.\n", strCommand, segList[1]);
        }
    }
    else if( strcmp( strCommand, "ls")==0 || strcmp( strCommand, "cat")==0 || strcmp( strCommand, "grep")==0 ){
        execvp( strCommand, segList);
    }
    else{
        fprintf( stderr, "%s: Command not found.\n", strCommand);
    }
}
void redirect( char *fileStr){
    int fd = open( fileStr, O_RDWR | O_CREAT , 0644);
    dup2( fd, STDOUT_FILENO);
    close( fd );
}
void pipeNum(waitPtr inNode){
    redirect( inNode->fileName ); // write in file
}
void earlyPipe(waitPtr *node, int pipe[2]){
    close(pipe[0]);
    int fd = open( (*node)->fileName, O_RDONLY , 0644 );
    if( fd==-1 ) fprintf( stderr, "open error in earlyPipe!\n");
    //dup2( pipe[1], fd ); // redirect fd to STDIN
    //close(pipe[1]);
    int n;
    char buffer[BUFFER_SIZE]="";
    do{
        n = read( fd, buffer, sizeof(buffer));
        write( pipe[1], buffer, n);
    }while(n!=0); // write the file content to the pipe
    close( pipe[1] );
    if( remove( (*node)->fileName )!=0 ) fprintf(stderr, "delete file error!\n"); // remove the file after used
    free( *node ); // free the list node
}
int isNumber(char *str){
    char *p;
    strtod( str, &p);
    return *p=='\0';
}
char *getName(int n) // generate a specail string for filename
{
    struct timeval tv;
    gettimeofday(&tv,NULL); // get current time
    char *string = (char*)malloc(40*sizeof(char));
    sprintf(string,"%d%ld%ld",n, tv.tv_sec, tv.tv_usec);
    return string;
}
waitPtr insertList(waitPtr *head, int n){
    waitPtr newPtr = (waitPtr)malloc( sizeof(waitNode) );
    newPtr->round = n;
    newPtr->fileName = getName(n);
    newPtr->next = NULL;
    if( *head==NULL ) *head = newPtr;
    else{
        waitPtr curPtr = *head;
        waitPtr prePtr = NULL;
        while( curPtr!=NULL && curPtr->round <= n ){
            prePtr = curPtr;
            curPtr = curPtr->next;
        }
        prePtr->next = newPtr;
        newPtr->next = curPtr;
    }
    return newPtr;
}
waitPtr popList(waitPtr *head){
    if(*head==NULL ) return NULL;
    int curRound = (*head)->round;
    waitPtr goalPtr = NULL;
    if( curRound==0 ){
        goalPtr = *head;
        *head = (*head)->next;
    }
    return goalPtr;
}
void updateList(waitPtr *head){
    if( *head==NULL ) return;
    waitPtr curPtr = *head;
    while( curPtr!=NULL ){
        curPtr->round = curPtr->round - 1;
        curPtr = curPtr->next;
    }
}
