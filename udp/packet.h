#define PACKET_SIZE 1024

typedef struct{ // structure of the package header
    struct sockaddr_in dest;
    unsigned int seqNum; // -1 represent FIN
    unsigned int dataSize;
} Header;

char *getFilename( char *path, unsigned int *filename_length){
    int i;
    *filename_length = 0;
    for(i=strlen(path);i>=0;--i){
        ++(*filename_length);
        if(path[i]=='/'||path[i]=='\\') return &path[i+1];
    }
    return path;
}
int isNumber(char *str){
    char *p;                                                                                                                                   
    strtod( str, &p);
    return *p=='\0';
}

const int pk_offset = sizeof(Header);
