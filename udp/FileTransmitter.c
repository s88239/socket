#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <fcntl.h>
#include <signal.h>
#include <pthread.h>
#include "packet.h"

#define BUFFER_SIZE 16
#define SERVER_PORT 9898
#define PATH_LENGTH 100
#define TIMEOUT 3
void sender( char * );
void askAgent();
void receiver();
void sendPackets();
void *receivePackets();
void resend();
void flushBuffer( char ** );
int writeInFile();
struct sockaddr_in makeSocket( char * , int );
void sender_FIN();
void receiver_FIN(struct sockaddr_in );
void reset();
int isReceived( char **, int, int );
const int STORE_ARR_SIZE = 2*BUFFER_SIZE;
socklen_t sock_length = sizeof(struct sockaddr_in);
struct sockaddr_in serverAddress;
char *packetArr[2*BUFFER_SIZE] = {NULL};
char *filename_str;
unsigned int file_length, filename_length;
struct sockaddr_in *agentAddress;
int *socket_fd, agentNum, first_packet_offset, flush_expect = 1;
int sock_fd, winSize = 1, threshold , winBase = 1, winNextseq = 1, totalPacketNum = 0, resendFlag = 0, done = 0, gap = -1, isGap = 0;
FILE *fptr;
Header pk_header, pk_ack;
int main(int argc, char *argv[])
{
    if( argc < 2 ) receiver(); // work as server to receive the file
    else if( argc==2 ) sender( argv[1] ); // work as client to sent the file
    else printf("You must follow these rules:\nSender: %s <Receiver IP>\nRecerver: %s\n", argv[0], argv[0]);
    return 0;
}
void sender( char *server_ip ){
    char path[PATH_LENGTH]="";
    pthread_t thread;
    pk_header.dest = serverAddress = makeSocket( server_ip, SERVER_PORT ); // store destination socket to forwarder agent
    askAgent();
    //if( agentNum<=0 ){ // connect receiver directly
        if( (sock_fd = socket(AF_INET, SOCK_DGRAM, 0) ) < 0 ){ // create a UDP socket
            fprintf( stderr, "Fail to create socket.\n");
            exit(1);
        }
    //}
    printf("Please input the file path you want to transmit.\n");
    if( signal(SIGALRM, resend) == SIG_ERR ){ // establish alarm signal
        fprintf( stderr, "Signal error!\n");
        return;
    }
    while( fgets(path, PATH_LENGTH, stdin)!=NULL ){ // get the path of the file from stdin
        path[strlen(path)-1] = 0; // remove the '\n'
        if( strcmp( path, "FIN" )==0 ){
            sender_FIN();
            //printf("PAUSE\n");
            pause();
            printf("End of program!\n");
            exit(0);
        }
        //printf("path = %s\n", path);
        if( (fptr = fopen( path, "rb" ) ) == NULL ){ // open the file
            fprintf( stderr, "File \"%s\" doesn't exist!\nPlease input the file path again.\n", path);
            continue;
        }
        filename_str = getFilename( path, &filename_length ); // store file name in the header
        //printf("File Name = %s\n", filename_str);
        //printf("filename_length = %d\n", filename_length);
        /*find the size of the file*/
        fseek( fptr, 0, SEEK_END );
        file_length = ftell( fptr ); // get the file length
        rewind( fptr );

        /*reset Go-Back-N arguments*/
        winSize = 1;
        winBase = 1;
        winNextseq = 1;
        totalPacketNum = 0;
        threshold = BUFFER_SIZE/2;

        pthread_create(&thread, NULL, receivePackets, NULL); // create a thread to receive packets
        sendPackets(); // send packets

        pthread_join(thread, NULL);
        //alarm(0); // turn off timer
        printf("Finish sending file\nPlease input the file path you want to transmit next.\n");
    }
    close(sock_fd);
}
void askAgent(){
    int i, port;
    char garbage, input[PATH_LENGTH] = "";
    struct sockaddr_in ipStruct;
    printf("How many agents you want to use?\n");
    scanf("%d%c", &agentNum, &garbage);
    socket_fd = (int *)malloc( agentNum * sizeof(int) );
    agentAddress = (struct sockaddr_in *)malloc( agentNum * sizeof(struct sockaddr_in) );
    for( i = 0 ; i < agentNum ; ++i ){
        printf("Please input the IP of Agent No.%d\n", i+1 );
        scanf("%s", input);
        printf("Input the port of Agent No.%d\n", i+1 );
        scanf("%d%c", &port, &garbage);
        agentAddress[i] = makeSocket( input, port );
        printf("Agent No.%d  %s:%d\n", i+1, inet_ntoa(agentAddress[i].sin_addr), htons(agentAddress[i].sin_port));
        if( (socket_fd[i] = socket(AF_INET, SOCK_DGRAM, 0) ) < 0 ){ // create a UDP socket
            fprintf( stderr, "Fail to create socket.\n");
            exit(1);
        }
    }
}
void receiver(){
    char *buffer[BUFFER_SIZE] = {NULL}; // create a buffer to store packets
    char *recvPacket; // receive packet in it from sender
    int receiveBytes = 0, bufferIdx = 0, current_offset;
    struct sockaddr_in clientAddress;
    if( (sock_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ){ // UDP
        fprintf( stderr, "Fail to create the socket.\n");
        exit(1);
    }
    bzero(&serverAddress, sizeof(serverAddress));
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(SERVER_PORT); // my server port
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    if( bind(sock_fd, (struct sockaddr *) &serverAddress, sizeof(serverAddress) ) < 0 ){
        fprintf( stderr, "Fail to bind.\n");
        exit(1);
    }
    while(1) {
        recvPacket = (char*)malloc( PACKET_SIZE * sizeof(char) );
        if( recvfrom(sock_fd, recvPacket, PACKET_SIZE, 0, (struct sockaddr *)&clientAddress, &sock_length) < 0 ){ // receive the package
            fprintf( stderr, "Can't read the datagram.\n");
            continue;
        }
        memcpy( &pk_header, recvPacket, sizeof(Header) ); // contract the header
        if( pk_header.seqNum==-1 ){ // receive FIN from sender
            printf("recv  FIN\n");
            receiver_FIN(clientAddress);
            printf("End of program!\n");
            exit(0);
        }
        if( done==2 || pk_header.seqNum - flush_expect < 0 ){ // already flushed
            printf("ignr  data #%d\n", pk_header.seqNum);
            //printf("Done = %d, seqNum = %d, lastSeqNum = %d\n", done, pk_header.seqNum, lastSeqNum );
            if( done==2 && pk_header.seqNum == totalPacketNum ){
                reset();
            }
        }
        else if( bufferIdx >= BUFFER_SIZE ){ // out of bound
            printf("drop  data #%d\n", pk_header.seqNum);
            flushBuffer( buffer);
            bufferIdx = 0;
            continue;
        }
        else{ // in bound
            if( pk_header.seqNum==1 ){ // first packet
                first_packet_offset = pk_offset;
                memcpy( &file_length , &recvPacket[first_packet_offset], sizeof(unsigned int) );
                first_packet_offset += sizeof(unsigned int);
                //printf("Received file_length = %d\n", file_length);
                memcpy( &filename_length, &recvPacket[first_packet_offset], sizeof(unsigned int) );
                first_packet_offset += sizeof(unsigned int);
                //printf("Received filename_length = %d\n", filename_length);
                filename_str = (char *)malloc( filename_length * sizeof(char) );
                memcpy( filename_str, &recvPacket[first_packet_offset], filename_length );
                //printf("Received filename_str = %s\n", filename_str);
                first_packet_offset += filename_length;
                char *newPath = (char*)malloc( (filename_length + 4) * sizeof(char) ); // create a new path
                strcpy( newPath, "cpy_");
                strcat( newPath, filename_str);
                if( (fptr = fopen( newPath, "wb+") )==NULL ){
                    fprintf( stderr,"Create file error!\n");
                    continue;
                }
                printf("Create a new file \"%s\"\n", newPath);
                free( newPath );
            }
            //if( expectSeq==pk_header.seqNum ) ++expectSeq;
            if( !isReceived(buffer, bufferIdx, pk_header.seqNum) ){ // if the packet is not received yet
                /*put packet in the buffer*/
                buffer[bufferIdx++] = recvPacket;
                ++totalPacketNum;
                printf("recv  data #%d\n", pk_header.seqNum);
                receiveBytes += pk_header.dataSize; // already received bytes
                //printf("dataSize = %d, receiveBytes = %d, fileSize = %d\n", pk_header.dataSize, receiveBytes, file_length);
            }
            else printf("ignr  data #%d\n", pk_header.seqNum); // the packet is already received
            if( receiveBytes == file_length ){ // receive file completely
                done = 1;
                //printf("Finish receiving file, fileSize = %d\n", file_length);
                //printf("Expect1 = %d\n", flush_expect);
                flushBuffer( buffer );
                fclose( fptr );
                bufferIdx = 0;
                receiveBytes = 0;
                //printf("Expect2 = %d\n", flush_expect);
                if( pk_header.seqNum < totalPacketNum ) done = 2;
                else if( pk_header.seqNum == totalPacketNum ){
                    reset();
                }
            }
        }
        /* send ack to sender */
        pk_ack.dest = pk_header.dest;
        pk_ack.seqNum = pk_header.seqNum;
        pk_ack.dataSize = 0;
        if( sendto( sock_fd, &pk_ack, sizeof(Header), 0, (struct sockaddr *)&clientAddress, sock_length) < 0 ){ // send ack
            fprintf( stderr, "Can't send the datagram" );
            continue;
        }
        printf("send  ack  #%d\n", pk_ack.seqNum);
    }
    close(sock_fd);
}
void sendPackets(){
    int nbytes, current_offset;
    socklen_t sock_length = sizeof(struct sockaddr_in);
    struct sockaddr_in *sendAddress;
    int send_sock;
    char packet_buf[PACKET_SIZE]="";
    while(1){
        if( winNextseq >= winBase + winSize ) alarm(TIMEOUT); // to keep from infinite loop
        while(winNextseq >= winBase + winSize ) ; // out of the window bound, do nothing
        bzero( packet_buf, PACKET_SIZE ); // clean buffer
        current_offset = pk_offset;
        pk_header.seqNum = winNextseq;
        if(pk_header.seqNum==1){ // first packet
            first_packet_offset = pk_offset;
            memcpy( &packet_buf[first_packet_offset], &file_length, sizeof(unsigned int) ); // store file length
            first_packet_offset += sizeof(unsigned int);
            //printf("filename_length = %d\n", filename_length);
            memcpy( &packet_buf[first_packet_offset], &filename_length, sizeof(unsigned int)); // store the length of file name
            first_packet_offset += sizeof(unsigned int);
            memcpy( &packet_buf[first_packet_offset], filename_str, filename_length); // store file name
            first_packet_offset += filename_length;
        }
        current_offset = (pk_header.seqNum==1) ? first_packet_offset : pk_offset;
        //printf("current offset = %d\n", current_offset);
        if( (nbytes = fread( &packet_buf[current_offset], 1, PACKET_SIZE - current_offset, fptr ) ) < 0 ){
            fprintf( stderr, "Read error!\n");
            return;
        }
        pk_header.dataSize = nbytes;
        //printf("Read bytes = %d\n", nbytes );
        memcpy( packet_buf, &pk_header, sizeof(Header) ); // copy head to buffer
        sendAddress = (agentNum==0) ? &serverAddress : &agentAddress[winNextseq % agentNum];
        send_sock = (agentNum == 0) ? sock_fd : socket_fd[winNextseq % agentNum];
        /*if( agentNum==0 ) printf("Send to receiver directly!\n");
        else printf("Send to Agent No.%d\n", winNextseq % agentNum + 1);*/
        if( sendto( send_sock, &packet_buf, current_offset + nbytes, 0, (struct sockaddr *)sendAddress, sock_length ) < 0 ) { // write to server through socket
            fprintf( stderr, "Fail to send the package.\n");
            exit(1);
        }
        if( winBase==winNextseq) alarm(TIMEOUT); // start timer
        if( resendFlag==1 ){ // resend happened
            resendFlag = 0; // turn off resend flag
            printf("resnd data #%d, winSize = %d\n", winNextseq, winSize);
        }
        else printf("send  data #%d, winSize = %d\n", winNextseq, winSize);
        ++winNextseq;
        if( feof(fptr) ){
            //printf("Finish tranmitting the file!\n");
            totalPacketNum = winNextseq-1;
            //printf("Total Packets = %d\n", totalPacketNum);
            while(!done) ; // not done yet
            if( done==1 ){
                fclose( fptr );
                return;
            }
            //printf("Done = %d\n", done);
        }
        done = 0;
        //printf("WinSize = %d, WinBase = %d, WinNextseq = %d\n", winSize, winBase, winNextseq);
    }
}
void *receivePackets(){
    int send_sock, countAck = 0;
    struct sockaddr_in *sendAddress;
    while(1){
        sendAddress = (agentNum==0) ? &serverAddress : &agentAddress[winBase % agentNum];
        send_sock = (agentNum == 0) ? sock_fd : socket_fd[winBase % agentNum];
        /*if( agentNum==0 ) printf("Receive from receiver directly!\n");
        else printf("Receive from Agent No.%d\n", winBase % agentNum + 1);*/
        if( recvfrom( send_sock, &pk_ack, sizeof(Header), 0, (struct sockaddr *)sendAddress, &sock_length ) < 0 ){
            fprintf( stderr, "Fail to receive package from server.\n");
            return;
        }
        printf("recv  ack  #%d\n", pk_ack.seqNum);
        ++countAck;
        if( winSize < threshold ){ // increase window size exponentially
            if( winSize == 1 ){
                winSize *= 2;
                countAck = 0;
            }
            if( countAck == 1 ) winSize*=2;
            if( countAck == winSize/2 ) countAck = 0;
        }
        else if( countAck>=winSize ){ // window size increase linearly
            ++winSize;
            countAck = 0;
        }
        //printf("countAck = %d, winSize = %d\n", countAck, winSize);
        if( winBase==pk_ack.seqNum ) ++winBase;
        if( winBase==winNextseq ) alarm(0); // turn off timer
        else alarm(TIMEOUT); // start timer
        if( totalPacketNum == winBase - 1 ){ // receive all acks
            //printf("receive all acks\n");
            alarm(0);
            done = 1;
            return;
        }
        //printf("recv: WinSize = %d, WinBase = %d, WinNextseq = %d\n", winSize, winBase, winNextseq);
    }
}
void resend(){
    printf("time  out    , threshold = %d\n", threshold);
    int seekBytes = ( winBase<=1 ) ? 0 : PACKET_SIZE - first_packet_offset + (winBase - 2) * ( PACKET_SIZE - pk_offset ) ;
    //printf("Resend! seekBytes = %d\n", seekBytes);
    fseek( fptr, seekBytes, SEEK_SET); // find the position in file for resended packet
    winNextseq = winBase; // retransmit
    threshold = winSize/2==0 ? 1 : winSize/2 ;
    winSize = 1; // set window size to 1 due to packet loss
    resendFlag = 1; // set flag for resending
    done = 2; // set flag to tell sender it should be resented
    alarm(TIMEOUT);
}
void flushBuffer( char **buf ){
    printf("flush\n");
    int i, j, current_offset, arr_offset;
    Header myHeader;
    for(i=0;i<BUFFER_SIZE;++i){ // flush buffer
        if( buf[i]==NULL ) return;
        memcpy( &myHeader, buf[i], sizeof(Header) );
        //printf("Flush seq #%d, dataSize = %d, flush_expect = %d\n", myHeader.seqNum, myHeader.dataSize, flush_expect);
        current_offset = (myHeader.seqNum==1) ? first_packet_offset : pk_offset;
        if( !isGap ){ // no gap occur
            if( myHeader.seqNum <= flush_expect ){
                if( fwrite( &buf[i][current_offset], 1, myHeader.dataSize, fptr ) < 0 ){ // write packet data in file
                    fprintf( stderr, "Write in file error!\n");
                }
                free( buf[i] );
                buf[i] = NULL;
                //printf("Write #%d from buffer into file\n", myHeader.seqNum);
                if( myHeader.seqNum==flush_expect ) ++flush_expect;
            }
            else{ // generate a gap
                //printf("Generate a gap! flush_expect = %d, #%d\n", flush_expect, myHeader.seqNum);
                gap = flush_expect;
                isGap = 1;
            }
        }
        //printf("Gap = %d, isGap = %d\n", gap, isGap );
        if( isGap ){
            arr_offset = myHeader.seqNum - flush_expect;
            packetArr[arr_offset] = buf[i];
            //printf("#%d arr_offset = %d\n", myHeader.seqNum, arr_offset);
            buf[i] = NULL;
            if( myHeader.seqNum==gap ){ // erase the gap
                isGap = 0;
                if( writeInFile()==0) return;
            }
        }
    }
}
int writeInFile(){
    int i, j, current_offset, sqn;
    Header myHeader;
    sqn = -1;
    for( i = 0 ; i < STORE_ARR_SIZE ; ++i ){ // write into file
        if( packetArr[i]==NULL ){ // new gap or no gap
            //printf("packetArr[%d] is NULL\n", i);
            isGap = 0; // default have no gap
            gap = -1;
            for( j = 0; j < STORE_ARR_SIZE - i; ++j ){ // move the following stored packets forward to 0
                if( packetArr[i+j]!=NULL ){ // still have a gap
                    //printf("packetArr[%d] is not NULL\n", i+j);
                    isGap = 1;
                    gap = sqn + 1;
                    packetArr[j] = packetArr[i+j];
                    packetArr[i+j] = NULL;
                }
            }
            //printf("After writing, gap = %d, isGap = %d\n", gap, isGap);
            break;
        }
        ++flush_expect;
        memcpy( &myHeader, packetArr[i], sizeof(Header));
        current_offset = (myHeader.seqNum==1) ? first_packet_offset : pk_offset;
        if( fwrite( &packetArr[i][current_offset], 1, myHeader.dataSize, fptr ) < 0 ){ // write packet data in file
            fprintf( stderr, "Write in file error!\n");
        }
        free( packetArr[i] );
        packetArr[i] = NULL;
        //printf("In packetArr, Write #%d into file\n", myHeader.seqNum);
        sqn = myHeader.seqNum;
        //printf("sqn = %d\n", sqn);
        if( done!=0 && sqn==totalPacketNum ) return 0;
    }
    return 1;
}
struct sockaddr_in makeSocket(char *ip, int port){
    struct sockaddr_in socket;
    char *ipAddress;
    struct hostent *host;
    struct in_addr **address;
    if( (host = gethostbyname(ip)) == NULL){
        fprintf(stderr, "gethostbyname error!\n");
        return;
    }
    address = (struct in_addr **)host->h_addr_list;
    ipAddress = inet_ntoa(*address[0]);

    bzero(&socket, sizeof(struct sockaddr_in));
    socket.sin_family = AF_INET;
    socket.sin_port = htons( port );
    inet_pton(AF_INET, ipAddress, &socket.sin_addr);
    return socket;
}
void sender_FIN(){
    pk_header.seqNum = -1; // sequence number -1 represent FIN
    if( sendto( sock_fd, &pk_header, sizeof(Header), 0, (struct sockaddr *)&serverAddress, sock_length ) < 0 ){ // send FIN to receiver directly
        fprintf( stderr, "Send FIN error!\n");
        return;
    }
    printf("send  FIN\n");
    if( recvfrom( sock_fd, &pk_ack, sizeof(Header), 0, (struct sockaddr *)&serverAddress, &sock_length ) < 0 ){
        fprintf( stderr, "Fail to receive package from server.\n");
        return;
    }
    if( pk_ack.seqNum==-1 ) printf("recv  ack  #FIN\n");
    else printf("recv  ack  #%d\n", pk_ack.seqNum);
    if( recvfrom( sock_fd, &pk_ack, sizeof(Header), 0, (struct sockaddr *)&serverAddress, &sock_length ) < 0 ){
        fprintf( stderr, "Fail to receive package from server.\n");
        return;
    }
    if( pk_ack.seqNum==-2 ) printf("recv  FIN\n");
    else printf("recv  ack  #%d\n", pk_ack.seqNum);
    alarm(TIMEOUT);
}
void receiver_FIN(struct sockaddr_in clientAddress){
    pk_ack.seqNum = -1;
    if( sendto( sock_fd, &pk_ack, sizeof(Header), 0, (struct sockaddr *)&clientAddress, sock_length ) < 0 ){ // send FIN to receiver directly
        fprintf( stderr, "Send FIN error!\n");
        return;
    }
    printf("send  ack  #FIN\n");
    pk_ack.seqNum = -2;
    if( sendto( sock_fd, &pk_ack, sizeof(Header), 0, (struct sockaddr *)&clientAddress, sock_length ) < 0 ){ // send FIN to receiver directly
        fprintf( stderr, "Send FIN error!\n");
        return;
    }
    printf("send  FIN\n");
}
int isReceived(char **buf, int bufIdx, int num ){
    int i;
    Header showHeader;
    for( i = 0 ; i < bufIdx ; ++i ){
        memcpy( &showHeader, buf[i], sizeof(Header) );
        if( showHeader.seqNum==num ) return 1; // have the same packet in the buffer
    }
    if( num < flush_expect){ // no gap, and already flushed packet was judged in first. Thus, we haven't receive it yet.
        //printf("num = %d < flush_expect = %d\n", num, flush_expect);
        return 1;
    }
    if( isGap && num < gap ){ // the packet is already writed in the file
        //printf("num = %d < gap = %d\n", num, gap);
        return 1;
    }
    if( isGap && packetArr[num - gap]!=NULL ){ // packet exceeding the expectSeq exists
        //printf("packetArr[%d]!=NULL, num = %d, gap = %d\n", num-gap,num,gap);
        return 1;
    }
    //printf("Not received\n");
    return 0;
}
void reset(){
    gap = -1;
    isGap = 0;
    done = 0;
    flush_expect = 1;
    totalPacketNum = 0;
    printf("Finish file trasmition!!\n");
}
