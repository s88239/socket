#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netdb.h>
#include <pthread.h>
#include "packet.h"

void *forward( void * );
int isDrop();
int client_fd, server_fd;
socklen_t length = sizeof( struct sockaddr_in );
Header pk_header, pk_ack;
struct sockaddr_in serverAddress, clientAddress;
double agent_lossrate;
int loss_pk_num = 0, total_pk_num = 0;
int main(int argc, char *argv[]){
    if( argc!=2 ){
        printf("You must follow the rule: %s <port>\n", argv[0]);
        return -1;
    }
    if( !isNumber(argv[1]) ){
        printf("You must input the port number with integer type!\n");
        return -1;
    }
    int agent_port = atoi(argv[1]);
    /*printf("Please input the port number.\n");
    scanf("%d", &agent_port);*/
    printf("Please define the loss rate of the agent. (0-100)\n");
    do{ // loss rate must in the range 0 - 100
        scanf("%lf", &agent_lossrate);
    }while( agent_lossrate > 100 || agent_lossrate < 0 );
    printf("Wait for connection...\n");
    srand(time(NULL)); // set the random
    pthread_t thread;
    char *ipAddress;; // receive packet in it from sender
    struct hostent *host;
    struct in_addr **address;
    /*work as server*/
    if( (client_fd = socket(AF_INET, SOCK_DGRAM, 0)) < 0 ){ // UDP
        fprintf( stderr, "Fail to create the socket.\n");
        exit(1);
    }
    bzero(&serverAddress, length);
    serverAddress.sin_family = AF_INET;
    serverAddress.sin_port = htons(agent_port); // my server port
    serverAddress.sin_addr.s_addr = htonl(INADDR_ANY);
    if( bind(client_fd, (struct sockaddr *) &serverAddress, sizeof(serverAddress) ) < 0 ){
        fprintf( stderr, "Fail to bind.\n");
        exit(1);
    }
    /*work as client*/
    if( (server_fd = socket(AF_INET, SOCK_DGRAM, 0) ) < 0 ){ // create a UDP socket
        fprintf( stderr, "Fail to create socket.\n");
        exit(1);
    }
    int sendData = 1, sendAck = 0;
    pthread_create(&thread, NULL, forward, (void *)&sendData); // create a thread to foward data
    forward( (void *)&sendAck);
    pthread_join(thread, NULL);
    return 0;
}
void *forward(void *arg){
    int isData = *(int *) arg;
    //printf("isData = %d\n", isData);
    int send_fd, recv_fd;
    struct sockaddr_in send_sock, recv_sock;
    send_fd = isData==1 ? client_fd : server_fd;
    recv_fd = isData==1 ? server_fd : client_fd;
    send_sock = isData==1 ? clientAddress : serverAddress;
    recv_sock = isData==1 ? serverAddress : clientAddress;
    
    //printf("start forward data\n");
    char *recvPacket;
    int nbytes;
    struct sockaddr_in target;
    while(1) {
        recvPacket = (char*)malloc( PACKET_SIZE * sizeof(char) );
        if( (nbytes = recvfrom(send_fd, recvPacket, PACKET_SIZE, 0, (struct sockaddr *)&send_sock, &length) ) < 0 ){ // receive the package
            fprintf( stderr, "Can't read the datagram.\n");
            continue;
        }
        if( isData==1 ){
            ++total_pk_num;
            memcpy( &pk_header, recvPacket, sizeof(Header) ); // contract the header
            printf("get   data #%d\n", pk_header.seqNum );
            target = pk_header.dest;
            pk_header.dest = send_sock;
            memcpy( recvPacket, &pk_header, sizeof(Header) );
            if( isDrop() ){
                ++loss_pk_num;
                printf("drop  data #%d, loss rate = %lf\n", pk_header.seqNum, (double)loss_pk_num/total_pk_num);
                continue;
            }
        }
        else{
            memcpy( &pk_ack, recvPacket, sizeof(Header) );
            printf("get   ack  #%d\n", pk_ack.seqNum );
            target = pk_ack.dest;
        }
        if( sendto( recv_fd, recvPacket, nbytes, 0, (struct sockaddr *)&target, length) < 0 ){
            fprintf( stderr, "Can't send the datagram" );
            continue;
        }
        if( isData==1 ) printf("fwd   data #%d, loss rate = %lf\n", pk_header.seqNum, (double)loss_pk_num/total_pk_num);
        else printf("fwd   ack  #%d\n", pk_ack.seqNum);
    }
    return 0;
}
int isDrop(){
    double randNum = (rand() % 10000)/100.0;
    //printf("RandNum = %lf\n", randNum);
    return randNum < agent_lossrate;
}
